# GuestInfoFrontend

The GuestInfoFrontend provides a web frontend that makes calls to GuestInfoBackend (a REST API server that implements an OpenAPI specification.) The users of the BNM system are clients of GuestInfoFrontend.

## Updated

This document was last updated on 11/4/2024.

## Project Folders

* .devcontainer 
  * This folder contains a configuration for opening the project in a container. 
* .gitlab
  * This folder contains the default.md file which defines the merge request description on GitLab.
* bin
  * This folder contains the utilities that help with building, server management, and committing within the project. 
* docs
  * This folder contains documentation that describes how to perform various maintenence and development tasks to navigate the system. 
* src
  * This folder contains all the executable code for the project. This includes the client code, client assets, package information, and docker configurations.
* testing
  * This folder contains manual test cases and an automated testing tool. The automated testing tool comes with a testing guide.
  
## Status

Landing Page is currently in Vue with functionality of buttons, data matches dictionary in backend. Connection to GuestInfoAPI v0.1.0 and restructured to match Thea's Pantry standards.

## Setting up the Development Environment

**Technologies needed to set up a Development Environment:**

* VSCode - VSCode can be downloaded for any operating system using the link provided below.
    
    [VSCode Setup](https://code.visualstudio.com/download)

* Docker Desktop - Docker Desktop can be installed for any operating system using the link provided below.
    
    [Docker Desktop Setup](https://docs.docker.com/get-started/get-docker/)
* Git - Git can be downloaded for any operating system using the link provided below.
    
    [Git Setup](https://git-scm.com/downloads)

**Instructions to set up a Development Environment:**

1.	Start Docker Desktop and keep it open in the background.
2.	Set up VSCode Extensions.
      * Open VSCode.
      * Use Ctrl-Shift-X to open the Extensions panel in Visual Studio Code.
      * Type `Dev Containers` in the search bar.
      * Install the Dev Containers extension by Microsoft ([Link](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-containers))
      * Once installed, close VSCode.

**Installing and running the project and its Dev Containers**

1.	Navigate to the GuestInfoFrontend project page [GuestInfoFrontend](https://gitlab.com/LibreFoodPantry/client-solutions/bear-necessities-market/guestinfosystem/guestinfofrontend) via GitLab and click the ‘Code’ drop down button  `Code -> Open in your IDE -> Visual Studio Code (HTTPS).`
2.	Allow the site to open VSCode.
3.	Select the location where you would like to store the project.
4.	VSCode will ask ‘Would you like to open the cloned repository?’ Select ‘Open’
5.	 A message will pop-up in the bottom right corner. Select "Reopen in container"
      * If you miss the pop-up, click the 2 blue arrows in the bottom-left corner and select "Reopen in Container".
6.	The project now should be opened in a container on your VSCode.
      * If you received an error setting up the container, open your Docker Desktop.
      * Go to `Settings -> Resources -> File Sharing`
      * Click the ‘+’ icon and select the project folder where you saved it.
      * Then click ‘Apply & restart’
      * Open VSCode again and retry opening in a container.
7.	Once the project is opened in a container on VSCode, open a new terminal
8.	To build the project, type in the command line: `./bin/build.sh`
9.	Launch the project using the `./bin/up.sh` command
10.	Click on the `Ports` tab
11.	Make the 10300 port be public (by clicking on the lock)
12.	Click on the globe which should open a new browser tab showing the application.
13.	For additional information on DevContainers please visit the [Development Environment](https://gitlab.com/LibreFoodPantry/client-solutions/bear-necessities-market/guestinfosystem/guestinfobackend/-/blob/main/docs/developer/development-environment.md?ref_type=heads) link.

## Tools

* Docker
* Vue
* JavaScript
* Yarn

## License Agreement 
Creative Commons Zero v1.0 Universal

## Development Infrastructure

* [Automated Testing](docs/developer/automated-testing.md)
* [Build System](docs/developer/build-system.md)
* [Cheat Sheet](docs/developer/cheat-sheet.md)
* [Continuous Integration](docs/developer/continuous-integration.md)
* [Dependency Management](docs/developer/dependency-management.md)
* [Development Environment](docs/developer/development-environment.md)
* [Documentation System](docs/developer/documentation-system.md)
* [Linting](docs/developer/linting.md)
* [Version Control System](docs/developer/version-control-system.md)
