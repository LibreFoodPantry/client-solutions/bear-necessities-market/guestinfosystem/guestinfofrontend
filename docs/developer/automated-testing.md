# Automated Testing

The system relies on Vitest for automated testing.
See [Testing Guide](/testing/automated/testing-guide.md) for instructions on how to perform automated testing.
