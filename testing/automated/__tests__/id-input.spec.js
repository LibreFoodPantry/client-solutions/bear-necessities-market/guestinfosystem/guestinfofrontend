/**
 * @vitest-environment happy-dom
 */

import { describe, it, expect } from 'vitest'
import { mount } from '@vue/test-utils'

// Import the Vue component to be tested
import IdInputComponent from '../../../src/components/id-input-component.vue'

// Initialize the test suite
describe('Initial Testing', () => {
    const wrapper = mount(IdInputComponent)
    // Test to assert proper initial values
    it('should have proper initial values', () => {
        // Mount the component
        
        // Assert initial values
        expect(wrapper.vm.idNum).toBe('')
        expect(wrapper.vm.idInputMode).toBe('manual')
        expect(wrapper.vm.swipeZerosSeen).toBe(0)
        expect(wrapper.vm.submitEnable).toBe(false)
    })
    it("assert vue component", ()=>{
        expect(wrapper.isVueInstance)
    })
})
describe('Function Testing', () => {
    // Test idNumChange method
    const wrapper = mount(IdInputComponent)
    it('should update idNum and submitEnable properly on idNumChange', () => {
        // Mount the component
        const wrapper = mount(IdInputComponent)
        // Simulate id number length < 9
        wrapper.vm.idNumChange({ keyCode: 49 }) // ASCII code for '1'
        expect(wrapper.vm.idNum).toBe('1')
        expect(wrapper.vm.submitEnable).toBe(false)

        // Simulate key input for 9 digits
        wrapper.vm.idNumChange({ keyCode: 50 }) // '2'
        expect(wrapper.vm.submitEnable).toBe(false)
        wrapper.vm.idNumChange({ keyCode: 51 }) // '3'
        expect(wrapper.vm.submitEnable).toBe(false)
        wrapper.vm.idNumChange({ keyCode: 52 }) // '4'
        expect(wrapper.vm.submitEnable).toBe(false)
        wrapper.vm.idNumChange({ keyCode: 53 }) // '5'
        expect(wrapper.vm.submitEnable).toBe(false)
        wrapper.vm.idNumChange({ keyCode: 54 }) // '6'
        expect(wrapper.vm.submitEnable).toBe(false)
        wrapper.vm.idNumChange({ keyCode: 55 }) // '7'
        expect(wrapper.vm.submitEnable).toBe(false)
        wrapper.vm.idNumChange({ keyCode: 56 }) // '8'
        expect(wrapper.vm.submitEnable).toBe(false)
        wrapper.vm.idNumChange({ keyCode: 57 }) // '9'
        expect(wrapper.vm.submitEnable).toBe(true)
    })

    // Test acceptDigits method
    it('should accept 9 digits properly', () => {
        // Mount the component
        const wrapper = mount(IdInputComponent)
        wrapper.setData({ idNum: '123' })
        // Simulate id number length < 9
        wrapper.vm.acceptDigits(49) // ASCII code for '1'
        expect(wrapper.vm.idNum).toBe('1231')
        expect(wrapper.vm.submitEnable).toBe(false)
        // Simulate key input for 9 digits 
        wrapper.setData({ idNum: '' }) // Reset idNum for the next simulation
        wrapper.vm.acceptDigits(49) // ASCII code for '1'
        wrapper.vm.acceptDigits(50) // ASCII code for '2'
        wrapper.vm.acceptDigits(51) // ASCII code for '3'
        wrapper.vm.acceptDigits(52) // ASCII code for '4'  
        wrapper.vm.acceptDigits(53) // ASCII code for '5'
        wrapper.vm.acceptDigits(54) // ASCII code for '6'
        wrapper.vm.acceptDigits(55) // ASCII code for '7'
        wrapper.vm.acceptDigits(56) // ASCII code for '8'
        wrapper.vm.acceptDigits(57) // ASCII code for '9'
        // Assert idNum and submitEnable values 
        expect(wrapper.vm.idNum).toBe('123456789') 
        expect(wrapper.vm.submitEnable).toBe(true) 
        expect(wrapper.vm.idInputMode).toBe('manual');
    }),
    // Test onClickSubmit method
    it('should emit sendIdEvent with idNum on onClickSubmit', () => {
        // Mount the component
        wrapper.setData({ idNum: '123456789' })
    // Simulate onClickSubmit
        wrapper.vm.onClickSubmit()
        expect(wrapper.vm.idNum).toBe('123456789') 
    // Assert that sendIdEvent was emitted
        expect(wrapper.emitted('sendIdEvent')[0]).toEqual(['123456789'])
    })
}) 
