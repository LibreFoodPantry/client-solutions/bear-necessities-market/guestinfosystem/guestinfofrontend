
/**
 * @vitest-environment happy-dom
 */


import { describe, it, expect } from 'vitest'
import bnmreports from '../../../src/components/bnm-reports.vue'
import { mount } from '@vue/test-utils'

//testing props
describe('bnmreports', () => {
  it('should have correct prop message', () => {
    const wrapper = mount(bnmreports, {props: {msg: 'Test message'}})
    expect(wrapper.props().msg).toBe('Test message')
  })
})

//testing object creation, must fail
describe('failing null getReport object', () => {
  const wrapper = mount(bnmreports)
  it('should fail', async () => {
    let reportObject = await wrapper.vm.getReport('2022-01-01', '2022-02-02')
    expect(reportObject).null
  })
  
  it('loads', ()=>{
    wrapper.vm.getReport('2022-01-01', '2022-02-02')
    expect(wrapper.vm.status).toBe('loaded')
  })
})

//testing object creation, must pass
describe('passing null getReport object', () => {
  it('should pass', async () => {
    const wrapper = mount(bnmreports)
    let reportObject = await wrapper.vm.getReport('2022-01-01', '2022-02-02')
    expect(reportObject).not.null
   
  })
})






