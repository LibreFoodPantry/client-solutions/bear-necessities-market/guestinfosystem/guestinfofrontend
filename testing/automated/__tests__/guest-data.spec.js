/**
 * @vitest-environment happy-dom
 */

import { describe, it, expect } from 'vitest'
import guestdata from '../../../src/components/guest-data-component.vue'
import { mount } from '@vue/test-utils'

function getCurrentDate() {
  const currentDate = new Date();
  const month = currentDate.getMonth() + 1; // Months are zero-based, so add 1
  const day = currentDate.getDate();
  const year = currentDate.getFullYear();
  return `${month}-${day}-${year}`;
}

describe('guestdata', () => {
  const wrapper = mount(guestdata)
  //console.log("Wrapper:\n",wrapper)
  console.log("Wrapper VM:\n",wrapper.vm)
  console.log("Guest Data:\n",wrapper.vm.guestData)
  console.log("Visit Data:\n",wrapper.vm.visitData)
  
  it('assert proper initial guestData values', () => {
    expect(wrapper.vm.guestData.BNMID).toBe('')
    expect(wrapper.vm.guestData.Resident).toBe('resident')
    expect(wrapper.vm.guestData.Grad_Year).toBe(1919)
    expect(wrapper.vm.guestData.Grad).toBe('UG')
    expect(wrapper.vm.guestData.Date).toBe(getCurrentDate())
    expect(wrapper.vm.guestData.Year_Issued).toBe(1919)

  })

  it('assert proper initial visitData values', () => {
    expect(wrapper.vm.visitData.BNMID).toBe('')
    expect(wrapper.vm.visitData.Date).toBe(getCurrentDate())
  })
  it('assert proper initial existingGuest value', () => {
    expect(wrapper.vm.existingGuest).toBe(true)
  })

  it('should have idNum', async () => {
    expect(wrapper.vm.idNum).toBe(undefined)
    wrapper.setProps({ idNum: 'HW1234567' });//this triggers fetchGuestData & causes an internal error
    await wrapper.vm.$nextTick();
    expect(wrapper.vm.idNum).toBe('HW1234567')
  })
  
  /* test functions*/
  it('submitGuestData', async () => {
    wrapper.setData({guestData:{
        BNMID: 'HW1234567',
        Residency: 'resident',
        Grad_Year: 2022,
        Grad: 'UG',
        Date: getCurrentDate()
    }})
    wrapper.vm.submitGuestData()
    expect(wrapper.vm.error).toBe(false)
    //guest was created in the previous function call
  })
  it('fetchGuestData', async () => {
    //initiated by changing the idNum due to the watcher on idNum
    //this call will set existingGuest and guestData values
    wrapper.vm.fetchGuestData()
    expect(wrapper.vm.existingGuest).toBe(true)
    expect(wrapper.vm.error).toBe(false)
  }) 

  it('removeGuestData', () => {
    //HTTP axios Delete (deletes the guest from the system)
    wrapper.vm.removeGuestData()
    expect(wrapper.vm.error).toBe(false)

  })

  it('recordVisit', () => {
    //sets the visitData.wneID to guestData.wneID
    wrapper.vm.recordVisit()
    expect(wrapper.vm.error).toBe(false)
    expect(wrapper.vm.visitData.wneID).toBe(wrapper.vm.guestData.wneID)
    //HTTP axios POST (logs the visit w visitData.wneID and date)
  })
  
  it('cancel', () => {
    //reloads the page 
  })

})
