# Automated Testing Guide

Follow these steps to run your tests using the `vitest` library:

## 1. Navigate to the Automated Testing Folder

Ensure that you are in the testing/automated folder.

## 2. Initialize Dependencies

Use the command `./suite.sh` to install dependencies and run the build.

## 3. Run the Test Suite

Use the command `./start.sh` in a new terminal to run the entire test suite.

## 3. Importing component

To add a test, you will need to import the component you want to test from the `src` folder. Here's an example of how to do this:

```javascript
import { describe, it, expect } from 'vitest'
import guestdata from '../../../src/components/guest-data-component.vue'
import { mount } from '@vue/test-utils'
```
## Creating Tests

Here's an example of how to create a test for the `IdInputComponent`:

```javascript
/**
 * @vitest-environment happy-dom
 */

import { describe, it, expect } from 'vitest'
import { mount } from '@vue/test-utils'

// Import the Vue component to be tested
import IdInputComponent from '../../../src/components/id-input-component.vue'

// Initialize the test suite
describe('Function Testing', () => {
    // Test idNumChange method
    const wrapper = mount(IdInputComponent)
    it('should update idNum and submitEnable properly on idNumChange', () => {
        // Mount the component
        const wrapper = mount(IdInputComponent)
        // Simulate id number length < 5
        wrapper.vm.idNumChange({ key: '1' })
        expect(wrapper.vm.idNum).toBe('1')
        expect(wrapper.vm.submitEnable).toBe(false)
        // Simulate key input for 5 digits
        wrapper.vm.idNumChange({ key: '2' })
        expect(wrapper.vm.submitEnable).toBe(false)
        wrapper.vm.idNumChange({ key: '3' })
        expect(wrapper.vm.submitEnable).toBe(false)
        wrapper.vm.idNumChange({ key: '4' })
        expect(wrapper.vm.submitEnable).toBe(false)
        wrapper.vm.idNumChange({ key: '5' })
        expect(wrapper.vm.submitEnable).toBe(true)
    })
})
//How to set data, for example from the guest-data component

wrapper.setData({guestData:{
    wneID: '12345',
    residency: 'resident',
    grad_year: 2025,
    grad: 'UG',
    date: "2024-3-11"
}
})


```

